﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using DocmailAPI2.CSharp.Demo.DMWS;

namespace DocmailAPI2.CSharp.Demo
{
    public enum AccountPaymentType
    {
        Invoice,
        Topup
    }

    class ApiHelpers
    {
        private readonly DMWSSoapClient Service = new DMWSSoapClient();
        private static List<ExtendedProperty> GetExtendedPropertyForMailingGuid(Guid gMailing)
        {
            return new List<ExtendedProperty>
      {
        new ExtendedProperty { PropertyName = "MailingGUID", PropertyValue = gMailing.ToString() }
      };
        }

        private readonly List<string> FinalStatuses = new List<string> { "Mailing submitted", "Mailing processed", "Partial processing complete" };

        /// <summary>
        /// defines the formatting of the return type - the example reponse handling in this sample assumes RETURN_FORMAT is used here
        /// </summary>
        private const string RETURN_FORMAT = "Text";
        private const int POLL_DELAY = 5000;

        public string AppName { get; }
        public string Username { get; }
        private string Password { get; }
        private AccountPaymentType AccountType { get; }
        public Action<string> OutputMessageHandler { get; }


        /// <summary>
        /// Basic constructor - stored basic repeatedly used parameters as Properties
        /// </summary>
        /// <param name="service"></param>
        /// <param name="appName"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="accountType"></param>
        /// 
        public ApiHelpers(string appName, string username, string password, AccountPaymentType accountType, Action<string> outputMessageHandler)
        {
            AppName = appName;
            Username = username;
            Password = password;
            AccountType = accountType;
            OutputMessageHandler = outputMessageHandler;
        }

        public string GetProcessingError(Guid gMailing)
        {
            OutputMessageHandler($"**** {nameof(GetProcessingError)} ****");
            return Service.ExtendedCall(Username, Password,
                                        MethodName: "GetProcessingError",
                                        Properties: GetExtendedPropertyForMailingGuid(gMailing).ToArray(),
                                        ReturnFormat: RETURN_FORMAT);
        }

        public void UserApproveMailing(Guid gMailing, string ReturnMessageFormat)
        {
            OutputMessageHandler($"**** {nameof(UserApproveMailing)} ****");
            Dictionary<string, string> oresults = null;

            oresults = ResultFormat.ToDictionary(Service.UserApproveMailing(Username, Password, gMailing, ReturnMessageFormat));
            ResultFormat.CheckError(oresults);
        }


        public Dictionary<string, string> WaitForStatus(Guid gMailingGUID, List<string> expectedStatuses, int iLoopWaitMilliSecs)
        {
            OutputMessageHandler($"**** {nameof(WaitForStatus)} ****");
            //The following code checks the status, looping until the proof is ready and then saves the proof file to disk
            while (true)
            {
                var rawResult = Service.GetStatus(Username, Password, gMailingGUID, RETURN_FORMAT);
                var results = ResultFormat.ToDictionary(rawResult);
                ResultFormat.CheckError(results);
                string currentStatus = results["Status"];
                OutputMessageHandler($"CurrentStatus: {currentStatus}");

                if (string.Equals(currentStatus, "error in processing", StringComparison.OrdinalIgnoreCase))
                    throw new ArgumentNullException(GetProcessingError(gMailingGUID));

                if (expectedStatuses.Any(x => string.Equals(currentStatus, x, StringComparison.OrdinalIgnoreCase)))
                {
                    OutputMessageHandler($"Expected Status Reached: {currentStatus}");
                    return results;
                }

                OutputMessageHandler($"Waiting: {iLoopWaitMilliSecs / 1000.00} seconds");
                // Wait one second between each poll
                Thread.Sleep(iLoopWaitMilliSecs);
            }
        }

        public void WaitForAndSaveProofLocally(Guid mailingGUID, string outputFile)
        {
            OutputMessageHandler($"**** {nameof(WaitForAndSaveProofLocally)} ****");
            //The following code checks the status, looping until the proof is ready and then saves the proof file to disk
            WaitForStatus(mailingGUID, FinalStatuses, POLL_DELAY);
            //Save the file to disk
            System.IO.File.WriteAllBytes(outputFile, Service.GetProofFile(Username, Password, mailingGUID, RETURN_FORMAT));
            OutputMessageHandler("Done");
        }

        public void GetPriceEstimate()
        {
            OutputMessageHandler($"**** {nameof(GetPriceEstimate)} ****");
            Dictionary<string, string> results = ResultFormat.ToDictionary(Service.GetPriceEstimate(Username: Username,
                                                                                                     Password: Password,
                                                                                                     DocumentType: "PostcardA5Right",
                                                                                                     IsMono: true,
                                                                                                     IsDuplex: false,
                                                                                                     DeliveryType: "Standard",
                                                                                                     DiscountCode: null,
                                                                                                     MinEnvelopeSize: null,
                                                                                                     EnvelopeGroup: null,
                                                                                                     Copies: 0,
                                                                                                     TotalSides: 2,
                                                                                                     StandardAddressCount: 1,
                                                                                                     SurchargeAddressCount: 1,
                                                                                                     WesternEuropeAddressCount: 1,
                                                                                                     RestOfTheWorldAddressCount: 1,
                                                                                                     ReturnFormat: RETURN_FORMAT));
            ResultFormat.CheckError(results);
        }

        public string GetBalance()
        {
            OutputMessageHandler($"**** {nameof(GetBalance)} (AccountType: {AccountType.ToString()}) ****");
            Dictionary<string, string> results = ResultFormat.ToDictionary(Service.GetBalance(Username: Username,
                                                                                              Password: Password,
                                                                                              AccountType: AccountType.ToString(),
                                                                                              ReturnFormat: RETURN_FORMAT));
            ResultFormat.CheckError(results);

            return results.Keys.Aggregate("", (agg, key) => $"{agg}{Environment.NewLine}{key}: {results[key]}");
        }



        public void GetProofImage(Guid gMailingGUID, string filePath)
        {
            OutputMessageHandler($"**** {nameof(GetProofImage)} ****");
            //The following code checks the status, looping until the proof is ready and then saves the proof file to disk
            // WaitForStatus(oresults, gMailingGUID, "Mailing submitted,Mailing processed,Partial processing complete", POLL_DELAY)
            // Save the file to disk
            System.IO.File.WriteAllBytes(filePath, Service.GetProofImage(Username, Password, gMailingGUID, 1, RETURN_FORMAT));
        }

        ///<summary>
        /// Wrapped call to create an PostCardA5 from sourcve image and message and save out a proof PDF
        /// Inclides an example of adding a single address programatically rather than loading in a mailing list file
        ///</summary>
        /// <param name="sourceImageFilePath">
        /// Path to image file for face of postcard
        /// adds a dummy address programatically as an example (rather than loading in a mailing list file)
        /// </param>
        /// <param name="contentMessage">
        /// textual content for the postcard
        /// supports merge tags e.g.      "Hi <<firstname>>." + System.Environment.NewLine + "Wish you were here!"
        /// </param>
        /// <param name="proofOutputFile"></param>
        /// <param name="mailingName"></param>
        /// <param name="mailingDescription"></param>
        /// <param name="emailOnError">
        /// Not requried, but useful for testing - will email on every order that errors during ProcessMailing
        /// </param>
        /// <param name="emailOnSuccess">
        /// Not requried, but useful for testing - will email on every successful order
        /// </param>
        /// <param name="httpPostOnSuccess">
        ///    callback targets httpPostOnSuccess & httpPostOnError
        ///    if passed in on ProcessMailing which triggers async processing call, a callback will be made as an HTTP POST to the error or success URL once the processing completes.
        ///    it is advisable to use callbacks instead or repeated polling calls to GetStats due to the greater efficiency of callbacks 
        ///    over polling loops for both the server and the client. 
        /// </param>
        /// <param name="httpPostOnError"></param>
        public void PostCardA5Test(string sourceImageFilePath, string contentMessage, string proofOutputFile, string mailingName,
                                   string mailingDescription, string emailOnError,
                                   string emailOnSuccess, string httpPostOnSuccess, string httpPostOnError)
        {
            OutputMessageHandler($"**** {nameof(PostCardA5Test)} ****");
            //Standard postcard template layouts:  "Postcard A5 left address", "Postcard A5 right address","Postcard A6 left address","Postcard A6 right address"
            string templateLayout = "Postcard A5 right address";
            //Current Document types: “GreetingCardA5”, “PostcardA5”, “PostcardA6”, “PostcardA5Right” (address on right), “PostcardA6Right” (address on right).
            string docType = "PostcardA5Right";
            //Bleed supplied: allows print to edge; assumes around 1.5mm additional image has been supplied on all sides that will be trimmed off after printing.
            bool bBleedSupplied = true;

            string imgFileName = "image.jpg";
            //Image fit options:  “Crop” (fits the shortest edge of the image into the card - fills more of the card) or “Resize to fit” (shrinks the image to fit the longest size - preserves whole image)
            string imageFitOption = "Crop";
            //Image Rotation (in degrees)
            double imgRotation = 0;


            //Declaring the client object (from service reference) in code:
            Dictionary<string, string> results = null;
            Guid mailingGUID;

            OutputMessageHandler(nameof(Service.CreateMailing));
            // Create a mailing
            results = ResultFormat.ToDictionary(Service.CreateMailing(Username: Username,
                                                                       Password: Password,
                                                                       CustomerApplication: AppName,
                                                                       ProductType: "Postcard",
                                                                       MailingName: mailingName,
                                                                       MailingDescription: mailingDescription,
                                                                       IsMono: false,
                                                                       IsDuplex: true,
                                                                       DeliveryType: "Standard",
                                                                       CourierDeliveryToSelf: false,
                                                                       DespatchASAP: true,
                                                                       DespatchDate: DateTime.MinValue,
                                                                       AddressNamePrefix: "",
                                                                       AddressNameFormat: "Full Name",
                                                                       DiscountCode: "",
                                                                       MinEnvelopeSize: "",
                                                                       ReturnFormat: RETURN_FORMAT));

            ResultFormat.CheckError(results);
            mailingGUID = ResultFormat.GetResultGUID(results, "MailingGUID");
            //get MailingGUID - required to continue the mailing

            OutputMessageHandler(nameof(Service.AddDesignerTemplate));
            //Add a template			- defines the size and layout of the postcard.
            results = ResultFormat.ToDictionary(Service.AddDesignerTemplate(Username: Username,
                                                                             Password: Password,
                                                                             MailingGUID: mailingGUID,
                                                                             TemplateLayout: templateLayout,
                                                                             DocumentType: docType,
                                                                             AddressFontCode: "",
                                                                             BleedSupplied: bBleedSupplied,
                                                                             Copies: 0,
                                                                             SkipPreviewImageGeneration: true,
                                                                             ReturnFormat: RETURN_FORMAT));
            ResultFormat.CheckError(results);

            OutputMessageHandler(nameof(Service.AddDesignerImage));
            //Add a picture           - PartDisplayname "Background photo" is the main image of a postcard
            results = ResultFormat.ToDictionary(Service.AddDesignerImage(Username: Username,
                                                                          Password: Password,
                                                                          MailingGUID: mailingGUID,
                                                                          PartDisplayName: "Background photo",
                                                                          FileName: imgFileName,
                                                                          FileData: System.IO.File.ReadAllBytes(sourceImageFilePath),
                                                                          ImageRotation: imgRotation,
                                                                          ImageFitOption: imageFitOption,
                                                                          ReturnFormat: RETURN_FORMAT));
            ResultFormat.CheckError(results);

            OutputMessageHandler(nameof(Service.AddDesignerText));
            //Add some message text   - PartDisplayname "Message" is the main text area of a postcard
            results = ResultFormat.ToDictionary(Service.AddDesignerText(Username: Username,
                                                                         Password: Password,
                                                                         MailingGUID: mailingGUID,
                                                                         PartDisplayName: "Message",
                                                                         TextContent: contentMessage,
                                                                         FontSize: 0,
                                                                         FontName: "",
                                                                         Bold: false,
                                                                         Italic: false,
                                                                         Underline: false,
                                                                         TextJustification: "",
                                                                         FontColourRed: 0,
                                                                         FontColourGreen: 0,
                                                                         FontColourBlue: 0,
                                                                         ReturnFormat: RETURN_FORMAT));
            ResultFormat.CheckError(results);

            // Add a mailing list		 - Alternatively use oService.AddAddress to construct individual addresses in code
            //oresults =  ResultFormat.ResultsToDictionary(oService.AddMailingListFile(Username, Password,   gMailingGUID, sAddressFileName, OpenFileAsByteArray(sAddressFilePath), "CSV", True, "", "", "", "", sReturnMessageFormat))
            //  ResultFormat.CheckError(oresults)

            OutputMessageHandler(nameof(Service.AddAddress));
            results = ResultFormat.ToDictionary(Service.AddAddress(Username: Username,
                                                                    Password: Password,
                                                                    MailingGUID: mailingGUID,
                                                                    Address1: "test", Address2: "testington way", Address3: "testaville", Address4: "", Address5: "", Address6: "",
                                                                    UseForProof: true,
                                                                    Title: "", FirstName: "Test", Surname: "Person", Fullname: "",
                                                                    JobTitle: "", CompanyName: "",
                                                                    Email: "", Telephone: "", DirectLine: "", Mobile: "", Facsimile: "",
                                                                    ExtraInfo: "", Notes: "",
                                                                    CustomerAddressID: "", CustomerImportID: "",
                                                                    StreamPages1: -1, StreamPages2: -1, StreamPages3: -1,
                                                                    Custom1: "", Custom2: "", Custom3: "", Custom4: "", Custom5: "",
                                                                    Custom6: "", Custom7: "", Custom8: "", Custom9: "", Custom10: "",
                                                                    ReturnFormat: RETURN_FORMAT));
            ResultFormat.CheckError(results);

            OutputMessageHandler(nameof(Service.ProcessMailing));
            //submit the mailing for processing, automatically approving the order        (Submit=true, PartialProcess=false)
            results = ResultFormat.ToDictionary(Service.ProcessMailing(Username,
                                                                        Password,
                                                                        mailingGUID,
                                                                        AppName,
                                                                        true,
                                                                        false,
                                                                        0,
                                                                        "",
                                                                        AccountType.ToString(),
                                                                        true,
                                                                        emailOnError,
                                                                        emailOnSuccess,
                                                                        httpPostOnError,
                                                                        httpPostOnSuccess,
                                                                        RETURN_FORMAT));
            ResultFormat.CheckError(results);

            OutputMessageHandler(nameof(WaitForAndSaveProofLocally));
            //wait around for the proof (not required)
            WaitForAndSaveProofLocally(mailingGUID, proofOutputFile);
        }

        /// <summary>
        /// Wrapped call to create an GreetingCard from source image and message and save out a proof PDF
        /// </summary>
        /// <param name="sourceImageFilePath"></param>
        /// <param name="contentMessage">
        /// textual content for the postcard
        /// supports merge tags e.g.      "Hi <<firstname>>." + System.Environment.NewLine + "Wish you were here!"
        /// </param>
        /// <param name="proofOutputFile"></param>
        /// <param name="mailingName"></param>
        /// <param name="mailingDescription"></param>
        /// <param name="addressFilePath">
        /// Path to CSV, TSV or Spreadsheet containg address data
        /// </param>
        /// <param name="emailOnError">
        /// Not requried, but useful for testing - will email on every order that errors during ProcessMailing
        /// </param>
        /// <param name="emailOnSuccess">
        /// Not requried, but useful for testing - will email on every successful order
        /// </param>
        /// <param name="httpPostOnSuccess"></param>
        /// <param name="httpPostOnError"></param>
        public void GreetingCardTest(string sourceImageFilePath, string contentMessage, string proofOutputFile, string mailingName,
                                     string mailingDescription, string addressFilePath, string emailOnError,
                                     string emailOnSuccess, string httpPostOnSuccess, string httpPostOnError)
        {
            OutputMessageHandler($"**** {nameof(GreetingCardTest)} ****");
            string productType = "Greeting Card";
            //Current postcard template layouts:  "Postcard A5 left address", "Postcard A5 right address","Postcard A6 left address","Postcard A6 right address"
            string templateLayout = "Landscape with border";
            //Current Document types: “GreetingCardA5”, “PostcardA5”, “PostcardA6”, “PostcardA5Right” (address on right), “PostcardA6Right” (address on right).
            string docType = "GreetingCardA5";
            //Bleed supplied: allows print to edge; assumes around 1.5mm additional image has been supplied on all sides that will be trimmed off after printing.
            bool bleedSupplied = true;

            string imgFileName = System.IO.Path.GetFileName(sourceImageFilePath);
            //Image fit options:  “Crop” (fits the shortest edge of the image into the card - fills more of the card) or “Resize to fit” (shrinks the image to fit the longest size - preserves whole image)
            string imageFitOption = "Crop";
            //Image Rotation (in degrees)
            double imgRotation = 0;


            //Declaring the client object (from service reference) in code:
            Dictionary<string, string> results = null;
            Guid mailingGUID;

            // Create a mailing
            results = ResultFormat.ToDictionary(Service.CreateMailing(Username: Username,
                                                                       Password: Password,
                                                                       CustomerApplication: AppName,
                                                                       ProductType: productType,
                                                                       MailingName: mailingName,
                                                                       MailingDescription: mailingDescription,
                                                                       IsMono: false,
                                                                       IsDuplex: true,
                                                                       DeliveryType: "First",
                                                                       CourierDeliveryToSelf: false,
                                                                       DespatchASAP: true,
                                                                       DespatchDate: DateTime.MinValue,
                                                                       AddressNamePrefix: "",
                                                                       AddressNameFormat: "Full Name",
                                                                       DiscountCode: "",
                                                                       MinEnvelopeSize: "",
                                                                       ReturnFormat: RETURN_FORMAT));

            ResultFormat.CheckError(results);
            mailingGUID = ResultFormat.GetResultGUID(results, "MailingGUID");
            //get MailingGUID - required to continue the mailing

            //Add a template			- defines the size and layout of the card.
            results = ResultFormat.ToDictionary(Service.AddDesignerTemplate(Username: Username,
                                                                             Password: Password,
                                                                             MailingGUID: mailingGUID,
                                                                             TemplateLayout: templateLayout,
                                                                             DocumentType: docType,
                                                                             AddressFontCode: "",
                                                                             BleedSupplied: bleedSupplied,
                                                                             Copies: 0,
                                                                             SkipPreviewImageGeneration: true,
                                                                             ReturnFormat: RETURN_FORMAT));
            ResultFormat.CheckError(results);

            //Add a picture           
            results = ResultFormat.ToDictionary(Service.AddDesignerImage(Username: Username,
                                                                          Password: Password,
                                                                          MailingGUID: mailingGUID,
                                                                          PartDisplayName: "Background photo",
                                                                          FileName: imgFileName,
                                                                          FileData: System.IO.File.ReadAllBytes(sourceImageFilePath),
                                                                          ImageRotation: imgRotation,
                                                                          ImageFitOption: imageFitOption,
                                                                          ReturnFormat: RETURN_FORMAT));
            ResultFormat.CheckError(results);

            results = ResultFormat.ToDictionary(Service.AddDesignerText(Username: Username,
                                                                         Password: Password,
                                                                         MailingGUID: mailingGUID,
                                                                         PartDisplayName: "Greeting",
                                                                         TextContent: contentMessage,
                                                                         FontSize: 0,
                                                                         FontName: "",
                                                                         Bold: false,
                                                                         Italic: false,
                                                                         Underline: false,
                                                                         TextJustification: "",
                                                                         FontColourRed: 0,
                                                                         FontColourGreen: 0,
                                                                         FontColourBlue: 0,
                                                                         ReturnFormat: RETURN_FORMAT));
            ResultFormat.CheckError(results);

            WaitForStatus(mailingGUID, new List<string> { "partial processing complete" }, POLL_DELAY);

            // Add a mailing list		 - Alternatively use oService.AddAddress to construct individual addresses in code
            results = ResultFormat.ToDictionary(Service.AddMailingListFile(Username: Username,
                                                                            Password: Password,
                                                                            MailingGUID: mailingGUID,
                                                                            FileName: addressFilePath,
                                                                            FileData: System.IO.File.ReadAllBytes(addressFilePath),
                                                                            DataFormat: "CSV",
                                                                            HasHeaders: true,
                                                                            SheetName: "",
                                                                            MappingDelimiter: "",
                                                                            MappingFixedWidthChars: "",
                                                                            MappingName: "",
                                                                            ReturnFormat: RETURN_FORMAT));
            ResultFormat.CheckError(results);

            //submit the mailing for processing, automatically approving the order        (Submit=true, PartialProcess=false)
            results = ResultFormat.ToDictionary(Service.ProcessMailing(Username: Username,
                                                                        Password: Password,
                                                                        MailingGUID: mailingGUID,
                                                                        CustomerApplication: AppName,
                                                                        Submit: true,
                                                                        PartialProcess: false,
                                                                        MaxPriceExVAT: 0,
                                                                        POReference: "",
                                                                        PaymentMethod: AccountType.ToString(),
                                                                        SkipPreviewImageGeneration: true,
                                                                        EmailSuccessList: emailOnError,
                                                                        EmailErrorList: emailOnSuccess,
                                                                        HttpPostOnSuccess: httpPostOnError,
                                                                        HttpPostOnError: httpPostOnSuccess,
                                                                        ReturnFormat: RETURN_FORMAT));
            ResultFormat.CheckError(results);

            //wait around for the proof (not required)
            WaitForAndSaveProofLocally(mailingGUID, proofOutputFile);

        }


        /// <summary>
        /// Simple submission of an A4 letted from a document, saving pout a proof PDF once completed
        /// </summary>
        /// <param name="proofOutputFile"></param>
        /// <param name="mailingName"></param>
        /// <param name="mailingDescription"></param>
        /// <param name="documentFilePath">
        /// Path to Word file or PDF as input document
        /// Files can contain merge tags or form fields
        /// </param>
        /// <param name="addressFilePath">
        /// Path to CSV, TSV or Spreadsheet containg address data
        /// </param>
        /// <param name="emailOnError">
        /// Not requried, but useful for testing - will email on every order that errors during ProcessMailing
        /// </param>
        /// <param name="emailOnSuccess">
        /// Not requried, but useful for testing - will email on every successful order
        /// </param>
        /// <param name="httpPostOnSuccess">
        ///    callback targets httpPostOnSuccess & httpPostOnError
        ///    if passed in on ProcessMailing which triggers async processing call, a callback will be made as an HTTP POST to the error or success URL once the processing completes.
        ///    it is advisable to use callbacks instead or repeated polling calls to GetStats due to the greater efficiency of callbacks 
        ///    over polling loops for both the server and the client. 
        /// </param>
        /// <param name="httpPostOnError"></param>
        public void LetterTest(string proofOutputFile, string mailingName, string mailingDescription,
                               string documentFilePath, string addressFilePath, string emailOnError, string emailOnSuccess,
                               string httpPostOnSuccess, string httpPostOnError)
        {
            OutputMessageHandler($"**** {nameof(LetterTest)} ****");
            string productType = "A4 Letter";
            string docType = "A4Letter";
            Guid mailingGUID;

            //Declaring the client object (from service reference) in code:
            // Create a mailing
            Dictionary<string, string> results = ResultFormat.ToDictionary(Service.CreateMailing(Username: Username,
                                                                       Password: Password,
                                                                       CustomerApplication: AppName,
                                                                       ProductType: productType,
                                                                       MailingName: mailingName,
                                                                       MailingDescription: mailingDescription,
                                                                       IsMono: false,
                                                                       IsDuplex: true,
                                                                       DeliveryType: "First",
                                                                       CourierDeliveryToSelf: false,
                                                                       DespatchASAP: true,
                                                                       DespatchDate: DateTime.MinValue,
                                                                       AddressNamePrefix: "",
                                                                       AddressNameFormat: "Full Name",
                                                                       DiscountCode: "",
                                                                       MinEnvelopeSize: "",
                                                                       ReturnFormat: RETURN_FORMAT));

            ResultFormat.CheckError(results);
            //get MailingGUID - required to continue the mailing
            mailingGUID = ResultFormat.GetResultGUID(results, "MailingGUID");

            //Add a template			
            results = ResultFormat.ToDictionary(Service.AddTemplateFile(Username: Username,
                                                                         Password: Password,
                                                                         MailingGUID: mailingGUID,
                                                                         TemplateName: "test template",
                                                                         FileName: documentFilePath,
                                                                         FileData: System.IO.File.ReadAllBytes(documentFilePath),
                                                                         DocumentType: docType,
                                                                         AddressedDocument: true,
                                                                         AddressFontCode: "",
                                                                         TemplateType: "",
                                                                         BackgroundName: "",
                                                                         CanBeginOnBack: false,
                                                                         NextTemplateCanBeginOnBack: true,
                                                                         ProtectedAreaPassword: "",
                                                                         EncryptionPassword: "",
                                                                         BleedSupplied: false,
                                                                         Copies: 1,
                                                                         Instances: 0,
                                                                         InstancePageNumbers: "0",
                                                                         CycleInstancesOnCopies: false,
                                                                         ReturnFormat: RETURN_FORMAT));
            ResultFormat.CheckError(results);


            // Add a mailing list		 - Alternatively use oService.AddAddress to construct individual addresses in code
            results = ResultFormat.ToDictionary(Service.AddMailingListFile(Username: Username,
                                                                            Password: Password,
                                                                            MailingGUID: mailingGUID,
                                                                            FileName: addressFilePath,
                                                                            FileData: System.IO.File.ReadAllBytes(addressFilePath),
                                                                            DataFormat: "CSV",
                                                                            HasHeaders: true,
                                                                            SheetName: "",
                                                                            MappingDelimiter: "",
                                                                            MappingFixedWidthChars: "",
                                                                            MappingName: "",
                                                                            ReturnFormat: RETURN_FORMAT));
            ResultFormat.CheckError(results);

            //submit the mailing for processing, automatically approving the order        (Submit=true, PartialProcess=false)
            results = ResultFormat.ToDictionary(Service.ProcessMailing(Username: Username,
                                                                        Password: Password,
                                                                        MailingGUID: mailingGUID,
                                                                        CustomerApplication: AppName,
                                                                        Submit: true,
                                                                        PartialProcess: false,
                                                                        MaxPriceExVAT: 0,
                                                                        POReference: "",
                                                                        PaymentMethod: AccountType.ToString(),
                                                                        SkipPreviewImageGeneration: true,
                                                                        EmailSuccessList: emailOnError,
                                                                        EmailErrorList: emailOnSuccess,
                                                                        HttpPostOnSuccess: httpPostOnError,
                                                                        HttpPostOnError: httpPostOnSuccess,
                                                                        ReturnFormat: RETURN_FORMAT));
            ResultFormat.CheckError(results);

            //wait around for the proof (not required)
            WaitForAndSaveProofLocally(mailingGUID, proofOutputFile);
        }

    }
}
