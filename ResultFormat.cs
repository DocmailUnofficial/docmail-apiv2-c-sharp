﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocmailAPI2.CSharp.Demo
{
  class ResultFormat
  {
    private const string ERROR_CODE = "Error code";
    private const string ERROR_CODE_STRING = "Error code string";
    private const string ERROR_MESSAGE = "Error message";

    //The following function convertes a text result string into a hash table for easy access to the return data: 
    public static Dictionary<string, string> ToDictionary(string resultData)
    {
      var resultValues = new Dictionary<string, string>();
      if (!string.IsNullOrWhiteSpace(resultData))
      {
        foreach (var line in resultData.Split(System.Environment.NewLine.ToCharArray()))
        {
          if (!string.IsNullOrWhiteSpace(line))
          {
            var values = line.Split(':');
            resultValues.Add(values[0], values[1].TrimStart());
          }
        }
      }
      return resultValues;
    }

    //The following function convertes a text result string into a hash table for easy access to the return data: 
    public static Dictionary<string, string> MultilineToDictionary(string resultData)
    {
      bool isFirst = true;
      Dictionary<string, string> results = new Dictionary<string, string>();
      if (!string.IsNullOrWhiteSpace(resultData))
      {
        foreach (string line in resultData.Split(System.Environment.NewLine.ToCharArray()))
        {
          if (isFirst)
          {
            //for list results, switch to standard result hash if the first item is an error line 
            if (line.StartsWith("Error"))
              return (ToDictionary(resultData));
            isFirst = false;
          }
          else if (!string.IsNullOrWhiteSpace(line))
          {
            var values = line.Split(',');
            results.Add(values[0], values[1]);
          }
        }
      }
      return results;
    }

    public static void CheckError(Dictionary<string, string> results)
    {
      if (results.ContainsKey(ERROR_CODE) && !string.IsNullOrEmpty(results[ERROR_CODE]))
      {
        throw new InvalidOperationException($"Error {results[ERROR_CODE]}: {results[ERROR_CODE_STRING]} - {results[ERROR_MESSAGE]}");
      }
    }

    public static Guid GetResultGUID(Dictionary<string, string> results, string field)
    {
      var resultGuid = new System.Guid(results[field]);
      if (resultGuid == Guid.Empty)
        throw new ArgumentNullException($"{field} Contained a blank or invalid GUID");
      return resultGuid;
    }

  }
}
