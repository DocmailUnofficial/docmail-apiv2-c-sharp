# README #

## What is this repository for? ##

### Quick summary
This is a .net console application as a simple demonstration for the [Docmail API v2](http://www.cfhdocmail.com/api), a SOAP-based wedbservice for sending physical mail via the Docmail printing and mailing (Hybrid Mail) service.

It covers simple examples of sending a Postcard, Greetings Card and A4 letter using C# Visual Studio.

I've published this on our unofficial channel as it's based on an API support/testing app not been through formal testing and as such may contain bugs, not be of production-standard, is unlikely to conform to accepted code patterns and standards, does not include logging or messaging etc. etc.

It is meant purely as a demonstrator/introduction to the Docmail APIv2; a starting point for other development and is provided without warranty.

It is available to be publicly forked/improved/progressed, or for the source to be used as the basis for other projects or simply to aid learning about Docmail APIv2 in C#.


## How do I get set up? ##

### Summary of set up

If you're running a copy of MS Visual Studio 2013, the integrated GIT options in the "team explorer"  should allow you to clone directly from BitBucket and you'll have a solution pretty much ready to go.

### Configuration

Within `PublicDemo.cs` and `ApiHelpers.cs` there are hard-coded values (for production, we'd expect these to be properly extracted to an appropriate configuration medium) that control aspects such as the credentials for use with the API, source file locations and some options used on the API calls.
The credentials variables within `Settings.settings` (or `App.config`) will require changing to your own values.

By default, the app.config points to the Docmail test API ([https://www.cfhdocmail.com/TestAPI2/DMWS.asmx](https://www.cfhdocmail.com/TestAPI2/DMWS.asmx)) and will therefore require credentials for the Docmail test system.  

If you don't have a Docmail test account, you can [sign up for free test account](https://www.cfhdocmail.com/test/signup.aspx).

### Dependencies

Solution and project files are in vs2013 format & will load in vs2013 onwards.

### Deployment instructions

As the default configuration is for the Docmail test system, the system.serviceModel.client.endpoint.address setting in the app.config file will need cahnging to point to the Live Docmail API at [https://www.cfhdocmail.com/LiveAPI2/DMWS.asmx](https://www.cfhdocmail.com/LiveAPI2/DMWS.asmx).

The live system is totally seperate from the test system, so also change the credentials over to a valid user in the live system.  If you don't have a live account, [sign up fro a free live account](https://www.cfhdocmail.com/live/signup.aspx).


## Who do I talk to? ##

### Other community or team contact

Docmail API development support is available via docmailsupport@cfh.com on a best endeavours basis (depending levels of demand for support).

# Sample Image Credit

Photo by [Hanny Naibaho](https://unsplash.com/@hannynaibaho?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/s/photos/postcard?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)