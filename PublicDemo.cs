﻿using System;

namespace DocmailAPI2.CSharp.Demo
{
  public class PublicDemo
  {
    //Docmail username and password should be used here
    //note: depending on the system.serviceModel.client.endpoint.address setting in the app.config file (pointing to the live or test Docmail system), 
    //      the credentials used here will need to vary, selecting the correct credentials for your user in either the live or test Docmail system.

    static ApiHelpers Api = new Demo.ApiHelpers(appName: "<your app name here> - CustomerTestApp C-Sharp",
                                                username: Properties.Settings.Default.Username, 
                                                password: Properties.Settings.Default.Password, 
                                                accountType: AccountPaymentType.Invoice,
                                                outputMessageHandler: ShowMessage);
    public static void Main()
    {
      try
      {
        Console.WriteLine($"App     : {Api.AppName}");
        Console.WriteLine($"Username: {Api.Username}");
        Console.WriteLine($"--------------------------");
        ShowMessage($"GetBalance: {Api.GetBalance()}");
        //basic examples provided for 3 different product types
        Api.PostCardA5Test(sourceImageFilePath: @"..\..\Images\hanny-naibaho-0YbeoQOX89k-unsplash.jpg",
                           contentMessage: "This is a Postcard for <<fullname>>",
                           proofOutputFile: $@"..\..\Proofs\proof-{DateTime.Now:yyyy-MM-dd_HH-mm-ss}.pdf",
                           mailingName: "This is a postcard mailing",
                           mailingDescription: "This is a postcard mailing",
                           emailOnError: "",
                           emailOnSuccess: "",
                           httpPostOnSuccess: "",
                           httpPostOnError: "");
        //GreetingCardTest();
        //LetterTest();

        //GetProof(Guid.NewGuid());
      }
      catch (Exception ex)
      {
        ShowMessage($"ERROR: {Environment.NewLine}{ex}");
      }
      Console.ReadLine();
    }


    private static void ShowMessage (string message)
    {
      Console.WriteLine($"{DateTime.Now:yyyy-MM-dd HH:mm:ss} - {message}");
    }

  }
}
